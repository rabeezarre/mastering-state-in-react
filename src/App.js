import "./styles/style.css";
import MainPage from "./pages/MainPage";
import CommunitySection from "./components/CommunitySection";
import { Routes, Route } from "react-router-dom";
import UserDetailsPage from "./pages/UserDetailsPage";
import NotFoundPage from "./pages/NotFoundPage";

function App() {
  return (
    <Routes>
      <Route path="/" element={<MainPage />} />
      <Route path="/community" element={<CommunitySection />} />
      <Route path="/community/:userId" element={<UserDetailsPage />} />
      <Route path="/not-found" element={<NotFoundPage />} />
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
}

export default App;
