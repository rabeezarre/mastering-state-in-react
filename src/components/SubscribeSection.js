import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectSubscriptionStatus } from "../redux/selectors";

export default function SubscribeSection() {
  const [email, setEmail] = useState("");
  const [isSubscribing, setIsSubscribing] = useState(false);
  const [isSubscribe, setIsSubscribe] = useState(true);
  const dispatch = useDispatch();
  const isSubscribedStatus = useSelector(selectSubscriptionStatus);
  // console.log(isSubscribedStatus);

  const handleSubscribe = async (event) => {
    event.preventDefault();
    setIsSubscribing(true);
    try {
      const response = await fetch("http://localhost:3000/subscribe", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        window.alert("Successfully subscribed!");
        setIsSubscribe(false);
        dispatch({ type: 'SET_SUBSCRIPTION_STATUS', payload: true });
      } else if (response.status === 422) {
        const data = await response.json();
        window.alert(data.error);
      } else {
        window.alert("Failed to subscribe.");
      }
    } catch (error) {
      console.error(error);
      window.alert("Failed to subscribe.");
    }
    setIsSubscribing(false);
  };

  const handleUnsubscribe = async (event) => {
    event.preventDefault();
    setIsSubscribing(true);
    try {
      const response = await fetch("http://localhost:3000/unsubscribe", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email }),
      });
      if (response.ok) {
        window.alert("Successfully unsubscribed!");
        setIsSubscribe(true);
        dispatch({ type: 'SET_SUBSCRIPTION_STATUS', payload: false });
      } else {
        window.alert("Failed to unsubscribe.");
      }
    } catch (error) {
      console.error(error);
      window.alert("Failed to unsubscribe.");
    }
    setIsSubscribing(false);
  };

  return (
    <section className="join-program app-section">
      <div className="join-program__container">
        <h2 className="join-program__title app-title">Join Our Program</h2>
        <h2 className="join-program__subtitle app-subtitle">
          Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </h2>
        <form className="join-program__form">
          <input
            className="join-program__form__input"
            placeholder="Email"
            type="email"
            name="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />

          {isSubscribe && (
            <button
              className="join-program__form__button"
              onClick={handleSubscribe}
              disabled={isSubscribing}
              style={{ opacity: isSubscribing ? 0.5 : 1 }}
            >
              <span>Subscribe</span>
            </button>
          )}

          {!isSubscribe && (
            <button
              className="join-program__form__button"
              onClick={handleUnsubscribe}
              disabled={isSubscribing}
              style={{ opacity: isSubscribing ? 0.5 : 1 }}
            >
              <span>Unsubscribe</span>
            </button>
          )}
        </form>
      </div>
    </section>
  );
}
