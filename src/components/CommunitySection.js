import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectUsers, selectVisibility } from "../redux/selectors";
import { fetchUserData } from "../redux/actions";
import { Link } from "react-router-dom";

export default function CommunitySection() {
  // const [communityList, setCommunityList] = useState([]);
  const dispatch = useDispatch();
  const communityList = useSelector(selectUsers);
  const isSectionVisible = useSelector(selectVisibility);

  // console.log(communityList);
  useEffect(() => {
    dispatch(fetchUserData()).then((result) => {
      // console.log(result.payload);
      dispatch({ type: "ADD_USERS", payload: result.payload });
    }).catch((error) => {
      console.log(error);
    });
  }, []);

  // const [isSectionVisible, setIsSectionVisible] = useState(true);

  const toggleSectionVisibility = () => {
    // setIsSectionVisible(!isSectionVisible);
    dispatch({type: "CHANGE_VISIBILITY", payload: !isSectionVisible});
  };

  return (
    <div>
      <section className="app-section">
        <div className="app-container-title-button">
          <h2 className="app-title">
            Big Community of <br /> People Like You{" "}
          </h2>
          <button onClick={toggleSectionVisibility} className="app-button">
            {isSectionVisible ? "Hide section" : "Show section"}
          </button>
        </div>

        {isSectionVisible && (
          <>
            <h3 className="app-subtitle">
              We’re proud of our products, and we’re really excited <br /> when
              we get feedback from our users.
            </h3>

            <div className="app-section__users-list">
              {communityList.map((member) => (
                <div className="card" key={member.id}>
                  <img className="card__img" src={member.avatar} alt="Avatar" />
                  <p className="card__description">
                    Lorem ipsum dolor sit amet, <br /> consectetur adipiscing
                    elit, sed do <br /> eiusmod tempor incididunt ut <br />{" "}
                    labore et dolor. <br />
                    <br />
                  </p>

                  <Link to={`/community/${member.id}`}>
                    <h3 className="card__full-name">
                    {member.firstName} {member.lastName}
                  </h3>
                  </Link>
                  
                  <h3 className="card__company-name">{member.position}</h3>
                </div>
              ))}
            </div>
          </>
        )}
      </section>
    </div>
  );
}
