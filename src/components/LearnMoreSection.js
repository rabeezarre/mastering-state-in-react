import React from "react";

export default function LearnMoreSection() {
  return (
    <section
      title="Section Learn More"
      description="Description for section learn more"
      className="app-section app-section--image-culture"
    >
      <h2 className="app-title">
        Learn more
        <br /> about our culture...
      </h2>
      <button className="app-section__button app-section__button--our-culture">
        ▶
      </button>
      <h3 className="app-subtitle">
        Duis aute irure dolor in <br />
        reprehenderit in voluptate velit esse
        <br /> cillum dolore eu fugiat nulla
      </h3>
    </section>
  );
}
