import { createStore, combineReducers, applyMiddleware } from 'redux';
import { usersReducer } from './usersReducer';
import { subscriptionReducer } from './subscriptionReducer';
import { visibilityReducer } from './visibilityReducer';
import thunkMiddleware from 'redux-thunk';


const rootReducer = combineReducers({
  users: usersReducer,
  subscription: subscriptionReducer,
  visibility: visibilityReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));


