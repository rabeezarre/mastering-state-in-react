const initialState = {
    isSubscribed: false,
  };
  
  export const subscriptionReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'SET_SUBSCRIPTION_STATUS':
        return {
          ...state,
          isSubscribed: action.payload,
        };
      default:
        return state;
    }
  };