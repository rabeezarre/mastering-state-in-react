const initialState = {
    isSectionsVisible: true,
  };
  
  export const visibilityReducer = (state = initialState, action) => {
    switch (action.type) {
      case 'CHANGE_VISIBILITY':
        return {
          ...state,
          isSectionsVisible: action.payload,
        };
      default:
        return state;
    }
  };