import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchUserData = createAsyncThunk(
  'users/fetchUserData',
  async (_, thunkAPI) => {
    try {
      const response = await axios.get('http://localhost:3000/community');
      return response.data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error.message);
    }
  }
);


export const addUser = (user) => {
  return {
    type: "ADD_USERS",
    payload: user,
  };
};

export const setSubscriptionStatus = (status) => {
  return {
    type: "SET_SUBSCRIPTION_STATUS",
    payload: status,
  };
};

export const showSection = (sectionId) => {
  return {
    type: "SHOW_SECTION",
    payload: sectionId,
  };
};

export const hideSection = (sectionId) => {
  return {
    type: "HIDE_SECTION",
    payload: sectionId,
  };
};
