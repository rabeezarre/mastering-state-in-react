const Card = (avatar, firstName, lastName, position) => {
  const fullNameString = `${firstName} ${lastName}`;
  const companyNameString = position.replace("at ", "at <br /> ");

  const card = document.createElement("div");
  card.classList.add("card");

  const img = document.createElement("img");
  img.classList.add("card__img");
  img.src = avatar;
  img.alt = "Avatar";

  const description = document.createElement("p");
  description.classList.add("card__description");
  description.innerHTML =
    "Lorem ipsum dolor sit amet, <br /> " +
    "consectetur adipiscing elit, sed do <br /> " +
    "eiusmod tempor incididunt ut <br /> " +
    "labore et dolor. <br /><br />";

  const fullName = document.createElement("h3");
  fullName.classList.add("card__full-name");
  fullName.innerHTML = fullNameString;

  const companyName = document.createElement("h3");
  companyName.classList.add("card__company-name");
  companyName.innerHTML = companyNameString;

  card.appendChild(img);
  card.appendChild(description);
  card.appendChild(fullName);
  card.appendChild(companyName);

  return card;
};

const createCommunitySection = () => {
  const communitySection = document.createElement("section");
  communitySection.classList.add("app-section");

  const h2Element = document.createElement("h2");
  h2Element.innerHTML = "Big Community of <br/> People Like You ";
  h2Element.classList.add("app-title");

  const h3Element = document.createElement("h3");
  h3Element.innerHTML =
    "We’re proud of our products, and we’re really excited <br /> when we get feedback from our users.";
  h3Element.classList.add("app-subtitle");

  const usersList = document.createElement("div");
  usersList.classList.add("app-section__users-list");

  communitySection.appendChild(h2Element);
  communitySection.appendChild(h3Element);
  communitySection.appendChild(usersList);

  async function getData() {
    try {
      const response = await fetch("http://localhost:3000/community");
      const result = await response.json();
      result.forEach((user) => {
        const card = Card(
          user.avatar,
          user.firstName,
          user.lastName,
          user.position
        );
        usersList.appendChild(card);
      });
    } catch (error) {
      alert(error);
    }
  }
  getData();

  document.addEventListener("DOMContentLoaded", function () {
    const footer = document.querySelector(".app-footer");
    footer.before(communitySection);
  });
};

export { createCommunitySection };
