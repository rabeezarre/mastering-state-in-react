import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from 'react-router-dom';


export default function UserDetailsPage() {
  const { userId } = useParams();
  const navigate = useNavigate();

  const [member, setMember] = useState([]);

  useEffect(() => {
    fetch("http://localhost:3000/community/" + userId)
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else if (response.status === 404) {
            navigate('/not-found');
        } else {
          throw new Error(`Request failed with status code ${response.status}`);
        }
      })
      .then((data) => {
        setMember(data);
      })
      .catch((error) => console.error(error));
  }, []);

  return (
    <>
      <div className="details-container" key={member.id}>
        <img className="card__img" src={member.avatar} alt="Avatar" />
        <p className="card__description">
          Lorem ipsum dolor sit amet, <br /> consectetur adipiscing elit, sed do{" "}
          <br /> eiusmod tempor incididunt ut <br /> labore et dolor. <br />
          <br />
        </p>

        <h3 className="card__full-name">
          {member.firstName} {member.lastName}
        </h3>

        <h3 className="card__company-name">{member.position}</h3>
      </div>
    </>
  );
}
