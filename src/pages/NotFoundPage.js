import React from "react";
import { Link } from "react-router-dom";

export default function NotFoundPage() {
  return (
    <div className="not-found-page">
      <div className="not-found-container">
        <div>
          <h1>Page Not Found</h1>
          <p>
            Looks like you've followed a broken link or entered a URL that
            doesn't exist on this site.
          </p>
          <Link to="/">← Back to Site</Link>
        </div>
      </div>
    </div>
  );
}
